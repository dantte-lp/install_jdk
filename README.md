# Установка Apache Tomcat в Linux

## Системные требования:

- Дистрибутив Linux: RHEL, Oracle Linux, SUSE Linux Enterprise Server, Debian, Ubuntu
- CPU: от 2 x vCPU
- RAM: от 4GB (зависит от Java приложения)
- DISK: от 20GB (желательно SSD)

## Установка на Oracle Linux 9

В примере используется дистрибутив Oracle Linux 9 UEK, скачать необходимый .iso образ можно по [ссылке](https://yum.oracle.com/oracle-linux-isos.html), есть в вашем окружении есть интернет, лучше использовать boot iso, который скачает последние версии пакетов из online репозитория Oracle Linux, например **OL9U2 x86_64-boot-uek.iso**.
При использовании boot дистрибутива вам необходимо будет добавить вручную репозитории.

1. Загрузите ваш сервер (виртуальный или физический) с .iso образа.
2.

## Настройка операционной системы

Перед установкой Apache Tomcat в Oracle Linux 9 UEK, необходимо произвести первоначальную настройку и установку зависимостей.

### Установка необходимых пакетов
- Установите **Extra Packages for Enterprise Linux**

`dnf -y install oracle-epel-release-el9 ; dnf -y upgrade`

- Включите отключенные репозитории
  `dnf config-manager --enable ol9_addons ol9_codeready_builder`
- Проверьте результат
  `dnf repolist --all`
- Установите необходимые для работы и зависимости
  `dnf -y install nano neovim openssl gnutls-utils bash-completion policycoreutils-python-utils policycoreutils-devel tree htop zip bmon apr-devel gcc make zlib-devel openssl-devel redhat-rpm-config nmap exa`
- Замените файл ~/.bashrc на содержимое

```shell
# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Path
if ! [[ "$PATH" =~ "/usr/sbin" ]]; then
    PATH="$PATH:/usr/sbin"
fi
if ! [[ "$PATH" =~ "/usr/local/bin" ]]; then
    PATH="$PATH:/usr/local/bin"
fi
if ! [[ "$PATH" =~ "/usr/local/sbin" ]]; then
    PATH="$PATH:/usr/local/sbin"
fi
if ! [[ "$PATH" =~ "$HOME/bin:" ]]; then
    PATH="$PATH:$HOME/bin"
fi
if ! [[ "$PATH" =~ "$HOME/.local/bin:" ]]; then
    PATH="$PATH:$HOME/.local/bin"
fi
export PATH

# Prompt
if [[ $USER == "root" ]]; then
        USER_COLOR=31
else
        USER_COLOR=32
fi
PS1="\[\e[${USER_COLOR}m\]\u\[\e[m\]@\[\e[34m\]\h\[\e[m\]:\[\e[35m\]\w\[\e[m\]\\$ "

# History options
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=10000
HISTFILESIZE=20000
HISTTIMEFORMAT='%d.%m.%Y %T '

# Aliases
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias xzegrep='xzegrep --color=auto'
alias xzfgrep='xzfgrep --color=auto'
alias xzgrep='xzgrep --color=auto'
alias zegrep='zegrep --color=auto'
alias zfgrep='zfgrep --color=auto'
alias zgrep='zgrep --color=auto'
alias ls='ls --color=auto'
alias ll='exa -bghHliSaUm'
alias lt='exa -bghHliSaUmT'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Bash completion
if [ -x "$(command -v kubectl)" ]; then
  source <(kubectl completion bash)
fi
if [ -x "$(command -v oc)" ]; then
  source <(oc completion bash)
fi
if [ -x "$(command -v helm)" ]; then
  source <(helm completion bash)
fi
if [ -x "$(command -v vault)" ]; then
  complete -C vault vault
fi

# k8s options
export KUBE_EDITOR="/bin/nano"
export DATASTORE_TYPE=kubernetes
export KUBECONFIG=~/.kube/config
EOF
```

## Установка JDK

Для функционирования Apache Tomcat необходимо установить один из JDK на сервер. Версия JDK зависит от вашего приложения, в продуктивных средах рекомендуется использовать LTS версию Java. На данный момент существуют JDK8, JDK11, JDK17, JDK21.
Вараинтов установки JDK на сервер Linux сещестсует несколько:
- Установка JDK из репозитория Oracle Linux
- Установка JDK с использованием исходных кодов (рекомендуемый)

### Установка JDK из репозитория Oracle Linux

#### Версия JDK8
```shell
dnf install java-1.8.0-openjdk --setopt install_weak_deps=false
```

#### Версия JDK11
```shell
dnf install java-11-openjdk --setopt install_weak_deps=false
```

#### Версия JDK17
```shell
dnf install java-17-openjdk --setopt install_weak_deps=false
```

Опция `--setopt install_weak_deps=false` указывает на то, чтобы при установке JDK не производилась установка рекомендуемых зависимостей.

**Установка без зависимостей**
```shell
root@tomcat-01:~# dnf install java-11-openjdk --setopt install_weak_deps=false
Last metadata expiration check: 3:12:35 ago on Sat 28 Oct 2023 06:16:54 PM +05.
Dependencies resolved.
==============================================================================================================================================================================================
 Package                                             Architecture                      Version                                             Repository                                    Size
==============================================================================================================================================================================================
Installing:
 java-11-openjdk                                     x86_64                            1:11.0.21.0.9-2.0.1.el9                             ol9_appstream                                444 k
Installing dependencies:
 alsa-lib                                            x86_64                            1.2.8-3.el9                                         ol9_appstream                                586 k
 avahi-libs                                          x86_64                            0.8-12.el9_2.1                                      ol9_baseos_latest                             71 k
 copy-jdk-configs                                    noarch                            4.0-3.el9                                           ol9_appstream                                 28 k
 cups-libs                                           x86_64                            1:2.3.3op2-16.el9_2.1                               ol9_baseos_latest                            264 k
 fontconfig                                          x86_64                            2.14.0-2.el9_1                                      ol9_appstream                                347 k
 freetype                                            x86_64                            2.10.4-9.el9                                        ol9_baseos_latest                            393 k
 graphite2                                           x86_64                            1.3.14-9.el9                                        ol9_baseos_latest                            100 k
 harfbuzz                                            x86_64                            2.7.4-8.el9                                         ol9_baseos_latest                            633 k
 java-11-openjdk-headless                            x86_64                            1:11.0.21.0.9-2.0.1.el9                             ol9_appstream                                 40 M
 javapackages-filesystem                             noarch                            6.0.0-4.el9                                         ol9_appstream                                 10 k
 libX11                                              x86_64                            1.7.0-7.el9                                         ol9_appstream                                648 k
 libX11-common                                       noarch                            1.7.0-7.el9                                         ol9_appstream                                350 k
 libXau                                              x86_64                            1.0.9-8.el9                                         ol9_appstream                                 36 k
 libXcomposite                                       x86_64                            0.4.5-7.el9                                         ol9_appstream                                 29 k
 libXext                                             x86_64                            1.3.4-8.el9                                         ol9_appstream                                 40 k
 libXi                                               x86_64                            1.7.10-8.el9                                        ol9_appstream                                 40 k
 libXrender                                          x86_64                            0.9.10-16.el9                                       ol9_appstream                                 28 k
 libXtst                                             x86_64                            1.2.3-16.el9                                        ol9_appstream                                 21 k
 libfontenc                                          x86_64                            1.1.3-17.el9                                        ol9_appstream                                 31 k
 libpng                                              x86_64                            2:1.6.37-12.el9                                     ol9_baseos_latest                            117 k
 libxcb                                              x86_64                            1.13.1-9.el9                                        ol9_appstream                                251 k
 lksctp-tools                                        x86_64                            1.0.19-2.el9                                        ol9_baseos_latest                            104 k
 lua                                                 x86_64                            5.4.4-3.el9                                         ol9_appstream                                201 k
 lua-posix                                           x86_64                            35.0-8.el9                                          ol9_appstream                                174 k
 mkfontscale                                         x86_64                            1.2.1-3.el9                                         ol9_appstream                                 37 k
 nspr                                                x86_64                            4.35.0-3.el9_2                                      ol9_appstream                                138 k
 nss                                                 x86_64                            3.90.0-3.el9_2                                      ol9_appstream                                717 k
 nss-softokn                                         x86_64                            3.90.0-3.el9_2                                      ol9_appstream                                388 k
 nss-softokn-freebl                                  x86_64                            3.90.0-3.el9_2                                      ol9_appstream                                334 k
 nss-sysinit                                         x86_64                            3.90.0-3.el9_2                                      ol9_appstream                                 19 k
 nss-util                                            x86_64                            3.90.0-3.el9_2                                      ol9_appstream                                 88 k
 ttmkfdir                                            x86_64                            3.0.9-65.el9                                        ol9_appstream                                 53 k
 tzdata-java                                         noarch                            2023c-1.el9                                         ol9_appstream                                522 k
 xml-common                                          noarch                            0.6.3-58.el9                                        ol9_appstream                                 43 k
 xorg-x11-fonts-Type1                                noarch                            7.5-33.el9                                          ol9_appstream                                526 k

Transaction Summary
==============================================================================================================================================================================================
Install  36 Packages

Total download size: 48 M
Installed size: 196 M
```

**Установка в зависимостями**

```shell
root@tomcat-01:~# dnf install java-11-openjdk 
Last metadata expiration check: 3:14:37 ago on Sat 28 Oct 2023 06:16:54 PM +05.
Dependencies resolved.
==============================================================================================================================================================================================
 Package                                                  Architecture                 Version                                                  Repository                               Size
==============================================================================================================================================================================================
Installing:
 java-11-openjdk                                          x86_64                       1:11.0.21.0.9-2.0.1.el9                                  ol9_appstream                           444 k
Installing dependencies:
 ModemManager-glib                                        x86_64                       1.20.2-1.el9                                             ol9_baseos_latest                       334 k
 adobe-source-code-pro-fonts                              noarch                       2.030.1.050-12.el9.1                                     ol9_baseos_latest                       849 k
 adwaita-cursor-theme                                     noarch                       40.1.1-3.el9                                             ol9_appstream                           686 k
 adwaita-icon-theme                                       noarch                       40.1.1-3.el9                                             ol9_appstream                            16 M
 alsa-lib                                                 x86_64                       1.2.8-3.el9                                              ol9_appstream                           586 k
 at-spi2-atk                                              x86_64                       2.38.0-4.el9                                             ol9_appstream                            92 k
 at-spi2-core                                             x86_64                       2.40.3-1.el9                                             ol9_appstream                           262 k
 atk                                                      x86_64                       2.36.0-5.el9                                             ol9_appstream                           378 k
 avahi-glib                                               x86_64                       0.8-12.el9_2.1                                           ol9_appstream                            13 k
 avahi-libs                                               x86_64                       0.8-12.el9_2.1                                           ol9_baseos_latest                        71 k
 bluez-libs                                               x86_64                       5.64-2.el9                                               ol9_baseos_latest                        83 k
 bubblewrap                                               x86_64                       0.4.1-6.el9                                              ol9_baseos_latest                        50 k
 cairo                                                    x86_64                       1.17.4-7.el9                                             ol9_appstream                           669 k
 cairo-gobject                                            x86_64                       1.17.4-7.el9                                             ol9_appstream                            18 k
 colord-libs                                              x86_64                       1.4.5-4.el9                                              ol9_appstream                           235 k
 copy-jdk-configs                                         noarch                       4.0-3.el9                                                ol9_appstream                            28 k
 cups-libs                                                x86_64                       1:2.3.3op2-16.el9_2.1                                    ol9_baseos_latest                       264 k
 exempi                                                   x86_64                       2.6.0-0.2.20211007gite23c213.el9                         ol9_appstream                           531 k
 exiv2-libs                                               x86_64                       0.27.5-2.el9                                             ol9_appstream                           780 k
 fdk-aac-free                                             x86_64                       2.0.0-8.el9                                              ol9_appstream                           325 k
 flac-libs                                                x86_64                       1.3.3-10.el9_2.1                                         ol9_appstream                           226 k
 flatpak-selinux                                          noarch                       1.12.7-2.el9                                             ol9_appstream                            22 k
 flatpak-session-helper                                   x86_64                       1.12.7-2.el9                                             ol9_appstream                            80 k
 fontconfig                                               x86_64                       2.14.0-2.el9_1                                           ol9_appstream                           347 k
 freetype                                                 x86_64                       2.10.4-9.el9                                             ol9_baseos_latest                       393 k
 fribidi                                                  x86_64                       1.0.10-6.el9.2                                           ol9_appstream                            94 k
 gdk-pixbuf2                                              x86_64                       2.42.6-3.el9                                             ol9_appstream                           584 k
 gdk-pixbuf2-modules                                      x86_64                       2.42.6-3.el9                                             ol9_appstream                            96 k
 geoclue2                                                 x86_64                       2.6.0-7.el9                                              ol9_appstream                           136 k
 giflib                                                   x86_64                       5.2.1-9.el9                                              ol9_appstream                            53 k
 glib-networking                                          x86_64                       2.68.3-3.el9                                             ol9_baseos_latest                       249 k
 graphene                                                 x86_64                       1.10.6-2.el9                                             ol9_appstream                            64 k
 graphite2                                                x86_64                       1.3.14-9.el9                                             ol9_baseos_latest                       100 k
 gsettings-desktop-schemas                                x86_64                       40.0-6.el9                                               ol9_baseos_latest                       761 k
 gsm                                                      x86_64                       1.0.19-6.el9                                             ol9_appstream                            39 k
 gstreamer1                                               x86_64                       1.18.4-4.el9                                             ol9_appstream                           1.5 M
 gstreamer1-plugins-base                                  x86_64                       1.18.4-5.el9                                             ol9_appstream                           2.1 M
 gtk-update-icon-cache                                    x86_64                       3.24.31-2.el9                                            ol9_appstream                            34 k
 harfbuzz                                                 x86_64                       2.7.4-8.el9                                              ol9_baseos_latest                       633 k
 hicolor-icon-theme                                       noarch                       0.17-13.el9                                              ol9_appstream                            70 k
 iso-codes                                                noarch                       4.6.0-3.el9                                              ol9_appstream                           4.0 M
 java-11-openjdk-headless                                 x86_64                       1:11.0.21.0.9-2.0.1.el9                                  ol9_appstream                            40 M
 javapackages-filesystem                                  noarch                       6.0.0-4.el9                                              ol9_appstream                            10 k
 jbigkit-libs                                             x86_64                       2.1-23.el9                                               ol9_appstream                            58 k
 json-glib                                                x86_64                       1.6.6-1.el9                                              ol9_baseos_latest                       214 k
 lcms2                                                    x86_64                       2.12-3.el9                                               ol9_appstream                           167 k
 libX11                                                   x86_64                       1.7.0-7.el9                                              ol9_appstream                           648 k
 libX11-common                                            noarch                       1.7.0-7.el9                                              ol9_appstream                           350 k
 libX11-xcb                                               x86_64                       1.7.0-7.el9                                              ol9_appstream                            12 k
 libXau                                                   x86_64                       1.0.9-8.el9                                              ol9_appstream                            36 k
 libXcomposite                                            x86_64                       0.4.5-7.el9                                              ol9_appstream                            29 k
 libXcursor                                               x86_64                       1.2.0-7.el9                                              ol9_appstream                            35 k
 libXdamage                                               x86_64                       1.1.5-7.el9                                              ol9_appstream                            27 k
 libXext                                                  x86_64                       1.3.4-8.el9                                              ol9_appstream                            40 k
 libXfixes                                                x86_64                       5.0.3-16.el9                                             ol9_appstream                            20 k
 libXft                                                   x86_64                       2.3.3-8.el9                                              ol9_appstream                            67 k
 libXi                                                    x86_64                       1.7.10-8.el9                                             ol9_appstream                            40 k
 libXinerama                                              x86_64                       1.1.4-10.el9                                             ol9_appstream                            15 k
 libXrandr                                                x86_64                       1.5.2-8.el9                                              ol9_appstream                            28 k
 libXrender                                               x86_64                       0.9.10-16.el9                                            ol9_appstream                            28 k
 libXtst                                                  x86_64                       1.2.3-16.el9                                             ol9_appstream                            21 k
 libXv                                                    x86_64                       1.0.11-16.el9                                            ol9_appstream                            19 k
 libXxf86vm                                               x86_64                       1.1.4-18.el9                                             ol9_appstream                            19 k
 libappstream-glib                                        x86_64                       0.7.18-4.el9                                             ol9_appstream                           431 k
 libasyncns                                               x86_64                       0.8-22.el9                                               ol9_appstream                            30 k
 libatomic                                                x86_64                       11.3.1-4.3.0.4.el9                                       ol9_baseos_latest                        31 k
 libcanberra                                              x86_64                       0.30-26.el9                                              ol9_appstream                           100 k
 libdatrie                                                x86_64                       0.2.13-4.el9                                             ol9_appstream                            33 k
 libepoxy                                                 x86_64                       1.5.5-4.el9                                              ol9_appstream                           246 k
 libexif                                                  x86_64                       0.6.22-6.el9                                             ol9_appstream                           462 k
 libfontenc                                               x86_64                       1.1.3-17.el9                                             ol9_appstream                            31 k
 libgexiv2                                                x86_64                       0.12.3-1.el9                                             ol9_appstream                            88 k
 libglvnd                                                 x86_64                       1:1.3.4-1.el9                                            ol9_appstream                           134 k
 libglvnd-egl                                             x86_64                       1:1.3.4-1.el9                                            ol9_appstream                            35 k
 libglvnd-glx                                             x86_64                       1:1.3.4-1.el9                                            ol9_appstream                           141 k
 libgsf                                                   x86_64                       1.14.47-5.el9                                            ol9_appstream                           302 k
 libgusb                                                  x86_64                       0.3.8-1.el9                                              ol9_baseos_latest                        58 k
 libgxps                                                  x86_64                       0.3.2-3.el9                                              ol9_appstream                            86 k
 libicu                                                   x86_64                       67.1-9.el9                                               ol9_baseos_latest                       9.6 M
 libiptcdata                                              x86_64                       1.0.5-9.el9                                              ol9_appstream                            73 k
 libjpeg-turbo                                            x86_64                       2.0.90-6.el9_1                                           ol9_appstream                           180 k
 libldac                                                  x86_64                       2.0.2.3-10.el9                                           ol9_appstream                            41 k
 libnotify                                                x86_64                       0.7.9-8.el9                                              ol9_appstream                            50 k
 libogg                                                   x86_64                       2:1.3.4-6.el9                                            ol9_appstream                            38 k
 libosinfo                                                x86_64                       1.10.0-1.el9                                             ol9_appstream                           345 k
 libpng                                                   x86_64                       2:1.6.37-12.el9                                          ol9_baseos_latest                       117 k
 libproxy                                                 x86_64                       0.4.15-35.el9                                            ol9_baseos_latest                        79 k
 librsvg2                                                 x86_64                       2.50.7-1.el9_2.1                                         ol9_appstream                           3.3 M
 libsbc                                                   x86_64                       1.4-9.el9                                                ol9_appstream                            45 k
 libsndfile                                               x86_64                       1.0.31-7.el9                                             ol9_appstream                           212 k
 libsoup                                                  x86_64                       2.72.0-8.el9                                             ol9_appstream                           465 k
 libstemmer                                               x86_64                       0-18.585svn.el9                                          ol9_appstream                            83 k
 libthai                                                  x86_64                       0.1.28-8.el9                                             ol9_appstream                           214 k
 libtheora                                                x86_64                       1:1.1.1-31.el9                                           ol9_appstream                           168 k
 libtiff                                                  x86_64                       4.4.0-8.el9_2                                            ol9_appstream                           202 k
 libtracker-sparql                                        x86_64                       3.1.2-3.el9_1                                            ol9_appstream                           354 k
 libusbx                                                  x86_64                       1.0.26-1.el9                                             ol9_baseos_latest                        81 k
 libvisual                                                x86_64                       1:0.4.0-34.el9                                           ol9_appstream                           152 k
 libvorbis                                                x86_64                       1:1.3.7-5.el9                                            ol9_appstream                           198 k
 libwayland-client                                        x86_64                       1.21.0-1.el9                                             ol9_appstream                            32 k
 libwayland-cursor                                        x86_64                       1.21.0-1.el9                                             ol9_appstream                            18 k
 libwayland-egl                                           x86_64                       1.21.0-1.el9                                             ol9_appstream                            12 k
 libwayland-server                                        x86_64                       1.21.0-1.el9                                             ol9_appstream                            41 k
 libwebp                                                  x86_64                       1.2.0-7.el9_2                                            ol9_appstream                           285 k
 libxcb                                                   x86_64                       1.13.1-9.el9                                             ol9_appstream                           251 k
 libxkbcommon                                             x86_64                       1.0.3-4.el9                                              ol9_appstream                           133 k
 libxshmfence                                             x86_64                       1.3-10.el9                                               ol9_appstream                            13 k
 lksctp-tools                                             x86_64                       1.0.19-2.el9                                             ol9_baseos_latest                       104 k
 low-memory-monitor                                       x86_64                       2.1-4.el9                                                ol9_appstream                            42 k
 lua                                                      x86_64                       5.4.4-3.el9                                              ol9_appstream                           201 k
 lua-posix                                                x86_64                       35.0-8.el9                                               ol9_appstream                           174 k
 mesa-libEGL                                              x86_64                       22.3.0-2.el9                                             ol9_appstream                           123 k
 mesa-libGL                                               x86_64                       22.3.0-2.el9                                             ol9_appstream                           168 k
 mesa-libgbm                                              x86_64                       22.3.0-2.el9                                             ol9_appstream                            37 k
 mesa-libglapi                                            x86_64                       22.3.0-2.el9                                             ol9_appstream                            49 k
 mesa-vulkan-drivers                                      x86_64                       22.3.0-2.el9                                             ol9_appstream                           7.4 M
 mkfontscale                                              x86_64                       1.2.1-3.el9                                              ol9_appstream                            37 k
 nspr                                                     x86_64                       4.35.0-3.el9_2                                           ol9_appstream                           138 k
 nss                                                      x86_64                       3.90.0-3.el9_2                                           ol9_appstream                           717 k
 nss-softokn                                              x86_64                       3.90.0-3.el9_2                                           ol9_appstream                           388 k
 nss-softokn-freebl                                       x86_64                       3.90.0-3.el9_2                                           ol9_appstream                           334 k
 nss-sysinit                                              x86_64                       3.90.0-3.el9_2                                           ol9_appstream                            19 k
 nss-util                                                 x86_64                       3.90.0-3.el9_2                                           ol9_appstream                            88 k
 openjpeg2                                                x86_64                       2.4.0-7.el9                                              ol9_appstream                           170 k
 opus                                                     x86_64                       1.3.1-10.el9                                             ol9_appstream                           200 k
 orc                                                      x86_64                       0.4.31-6.el9                                             ol9_appstream                           188 k
 osinfo-db                                                noarch                       20221130-1.0.2.el9                                       ol9_appstream                           1.2 M
 osinfo-db-tools                                          x86_64                       1.10.0-1.el9                                             ol9_appstream                            96 k
 ostree-libs                                              x86_64                       2023.1-7.el9_2                                           ol9_appstream                           441 k
 pango                                                    x86_64                       1.48.7-3.el9                                             ol9_appstream                           314 k
 pipewire-libs                                            x86_64                       0.3.47-4.el9_2                                           ol9_appstream                           1.7 M
 pixman                                                   x86_64                       0.40.0-5.el9                                             ol9_appstream                           270 k
 polkit                                                   x86_64                       0.117-11.0.1.el9                                         ol9_baseos_latest                       183 k
 polkit-libs                                              x86_64                       0.117-11.0.1.el9                                         ol9_baseos_latest                       8.3 M
 polkit-pkla-compat                                       x86_64                       0.1-21.el9                                               ol9_baseos_latest                        54 k
 poppler                                                  x86_64                       21.01.0-14.el9                                           ol9_appstream                           1.1 M
 poppler-data                                             noarch                       0.4.9-9.el9                                              ol9_appstream                           2.1 M
 poppler-glib                                             x86_64                       21.01.0-14.el9                                           ol9_appstream                           152 k
 pulseaudio-libs                                          x86_64                       15.0-2.el9                                               ol9_appstream                           726 k
 pulseaudio-utils                                         x86_64                       15.0-2.el9                                               ol9_appstream                            82 k
 rtkit                                                    x86_64                       0.11-28.el9                                              ol9_appstream                            69 k
 shared-mime-info                                         x86_64                       2.1-5.el9                                                ol9_baseos_latest                       385 k
 sound-theme-freedesktop                                  noarch                       0.8-17.el9                                               ol9_appstream                           407 k
 totem-pl-parser                                          x86_64                       3.26.6-2.el9                                             ol9_appstream                           238 k
 tracker                                                  x86_64                       3.1.2-3.el9_1                                            ol9_appstream                           616 k
 ttmkfdir                                                 x86_64                       3.0.9-65.el9                                             ol9_appstream                            53 k
 tzdata-java                                              noarch                       2023c-1.el9                                              ol9_appstream                           522 k
 upower                                                   x86_64                       0.99.13-2.el9                                            ol9_appstream                           188 k
 vulkan-loader                                            x86_64                       1.3.239.0-1.el9                                          ol9_appstream                           143 k
 webkit2gtk3-jsc                                          x86_64                       2.38.5-1.el9_2.3                                         ol9_appstream                           3.2 M
 webrtc-audio-processing                                  x86_64                       0.3.1-8.el9                                              ol9_appstream                           310 k
 wireplumber                                              x86_64                       0.4.8-1.0.1.el9                                          ol9_appstream                           123 k
 wireplumber-libs                                         x86_64                       0.4.8-1.0.1.el9                                          ol9_appstream                           335 k
 xdg-dbus-proxy                                           x86_64                       0.1.3-1.el9                                              ol9_appstream                            41 k
 xdg-desktop-portal                                       x86_64                       1.12.4-1.el9                                             ol9_appstream                           453 k
 xkeyboard-config                                         noarch                       2.33-2.el9                                               ol9_appstream                           1.1 M
 xml-common                                               noarch                       0.6.3-58.el9                                             ol9_appstream                            43 k
 xorg-x11-fonts-Type1                                     noarch                       7.5-33.el9                                               ol9_appstream                           526 k
Installing weak dependencies:
 abattis-cantarell-fonts                                  noarch                       0.301-4.el9                                              ol9_appstream                           376 k
 dconf                                                    x86_64                       0.40.0-6.el9                                             ol9_appstream                           121 k
 exiv2                                                    x86_64                       0.27.5-2.el9                                             ol9_appstream                           1.0 M
 flatpak                                                  x86_64                       1.12.7-2.el9                                             ol9_appstream                           1.8 M
 gtk3                                                     x86_64                       3.24.31-2.el9                                            ol9_appstream                           5.0 M
 libcanberra-gtk3                                         x86_64                       0.30-26.el9                                              ol9_appstream                            39 k
 libproxy-webkitgtk4                                      x86_64                       0.4.15-35.el9                                            ol9_appstream                            21 k
 p11-kit-server                                           x86_64                       0.24.1-2.el9                                             ol9_appstream                           202 k
 pipewire                                                 x86_64                       0.3.47-4.el9_2                                           ol9_appstream                            48 k
 pipewire-alsa                                            x86_64                       0.3.47-4.el9_2                                           ol9_appstream                            58 k
 pipewire-jack-audio-connection-kit                       x86_64                       0.3.47-4.el9_2                                           ol9_appstream                           132 k
 pipewire-pulseaudio                                      x86_64                       0.3.47-4.el9_2                                           ol9_appstream                            23 k
 tracker-miners                                           x86_64                       3.1.2-3.el9                                              ol9_appstream                           1.0 M
 xdg-desktop-portal-gtk                                   x86_64                       1.12.0-3.el9                                             ol9_appstream                           162 k

Transaction Summary
==============================================================================================================================================================================================
Install  173 Packages

Total download size: 141 M
Installed size: 497 M
```

### Установка JDK с использованием архива с бинарными файлами (рекомендуемый)

При установке JDK с использованием бинарных файлов вы можете установить любую из сборок OpenJDK, которая существует на рынке, вот основные реализации:

| Сборка | Организация | LTS | Разрешительная лицензия | Коммерческая поддержка | 
|--|--| -- | -- |--| 
| Eclipse Temurin | Eclipse Foundation | Yes | Yes | Optional | 
| Amazon Corretto | Amazon | Yes | Yes | Optional |
| Azul Zulu | Azul Systems | Yes | Yes | Optional |
| GraalVM | Oracle | Yes | No | Yes | 
| Microsoft Build of OpenJDK | Microsoft | Yes | Yes | Optional |
| Oracle Java SE | Oracle | Yes | No | Yes | 
| Red Hat build of OpenJDK | Red Hat | Yes | Yes | Yes | 

Выбор сборки зависит от ваших задач, рекомендуется использовать **Eclipse Temurin**, поскольку эта сборка является полностью бесплатной, не требует отчисления платежей за использование, и поддерживается крупной независимой европейской некоммерческой корпорацией.

Как пример, мы установим все перечисленные выше сборки, чтобы показать процесс установки и обновления JDK. В качестве основной версии мы возьмем JDK21.

#### Установка JDK Eclipse Temurin

Чтобы найти подходящую для вас версию JDK, пройтиде на официальный сайт [Eclipse Temurin](https://adoptium.net/temurin/releases/), выберите **Operating System**: Linux, **Architecture**: x64, **Package Type**: JDK, **Version**: 21 - LTS.
Ниже в таблице вы увидите актуальную версию JDK вместе с ссылкой на скачивание архива .tar.gz


Переходим в `/tmp` и скачиваем архив с бинарными файлами JDK под необходимую платформу
```shell
cd /tmp && curl -JLO https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.1%2B12/OpenJDK21U-jdk_x64_linux_hotspot_21.0.1_12.tar.gz
```

Создаем директорию `/etc/java/` и разархивируем в неё бинарные файлы JDK
```shell
mkdir /etc/java && tar -zxvf OpenJDK21U-jdk_x64_linux_hotspot_21.0.1_12.tar.gz -C /etc/java
```

Проверяем результат
```shell
root@tomcat-01:/tmp# ll /etc/java/
   inode Permissions Links Size Blocks User Group Date Modified Date Created Name
34661891 drwxr-xr-x@     9    -      - root root  19 Oct 01:36  28 Oct 22:07 jdk-21.0.1+12
root@tomcat-01:/tmp# ll /etc/java/jdk-21.0.1+12/
   inode Permissions Links  Size Blocks User Group Date Modified Date Created Name
51187876 drwxr-xr-x@     2     -      - root root  19 Oct 01:36  28 Oct 22:07 bin
50851952 drwxr-xr-x@     5     -      - root root  19 Oct 01:35  28 Oct 22:07 conf
34662029 drwxr-xr-x@     3     -      - root root  19 Oct 01:35  28 Oct 22:07 include
  329885 drwxr-xr-x@     2     -      - root root  19 Oct 01:35  28 Oct 22:07 jmods
  329743 drwxr-xr-x@    71     -      - root root  19 Oct 01:35  28 Oct 22:07 legal
  545979 drwxr-xr-x@     5     -      - root root  19 Oct 01:36  28 Oct 22:07 lib
16778511 drwxr-xr-x@     3     -      - root root  19 Oct 01:35  28 Oct 22:07 man
34662037 .rw-r--r--@     1 2.3Ki      8 root root  19 Oct 01:36  28 Oct 22:07 NOTICE
34662036 .rw-r--r--@     1 1.6Ki      8 root root  19 Oct 01:35  28 Oct 22:07 release
```

Создаем символическую ссылку на необходимую нам версию JDK, на которую будет ссылаться системная переменная `$JAVA_HOME`
```shell
cd /etc/java/ && ln -s jdk-21.0.1+12 current
```

Проверяем результат
```shell
root@tomcat-01:/etc/java# ll
   inode Permissions Links Size Blocks User Group Date Modified Date Created Name
16779617 lrwxrwxrwx@     1   13      0 root root  28 Oct 22:11  28 Oct 22:11 current -> jdk-21.0.1+12
34661891 drwxr-xr-x@     9    -      - root root  19 Oct 01:36  28 Oct 22:07 jdk-21.0.1+12
```

Создайте скрипт иницилизации переменной среды

```shell
touch /etc/profile.d/jdk_env.sh
```

Внесите в него следующие данные

```shell
export PATH=/etc/java/current/bin:$PATH
export JAVA_HOME=/etc/java/current
export JRE_HOME=$JAVA_HOME
export CLASSPATH=.:${JAVA_HOME}/lib
```

Для иницилизации скрипта запустите команду, либо перезайдите на сервер
```
source /etc/profile.d/jdk_env.sh
```

Проверьте версию JDK, чтобы убедиться в правильной установке

```shell
java --version
```

Вывод команды

```shell
root@tomcat-01:/etc/java# java --version
openjdk 21.0.1 2023-10-17 LTS
OpenJDK Runtime Environment Temurin-21.0.1+12 (build 21.0.1+12-LTS)
OpenJDK 64-Bit Server VM Temurin-21.0.1+12 (build 21.0.1+12-LTS, mixed mode, sharing)
```

#### Обновление или изменение версии JDK

Если вам потребуется использовать другую версию JDK, либо сменить сборку, вам достаточно скачать новый архив с бинарными файлами и распаковать его в директорию `/etc/java/`, а после обновить символическую ссылку на новую версию

Как пример, ниже показано как добавить другие сборки и версии JDK.

##### Установка Amazon Corretto

```shell
cd /tmp && curl -JLO https://corretto.aws/downloads/latest/amazon-corretto-21-x64-linux-jdk.tar.gz
tar -zxvf amazon-corretto-21-x64-linux-jdk.tar.gz -C /etc/java && \
cd /etc/java && \
rm -f current && \
ln -s amazon-corretto-21.0.1.12.1-linux-x64 current 
```

Проверка новой версии JDK
```shell
root@tomcat-01:/etc/java# java --version
openjdk 21.0.1 2023-10-17 LTS
OpenJDK Runtime Environment Corretto-21.0.1.12.1 (build 21.0.1+12-LTS)
OpenJDK 64-Bit Server VM Corretto-21.0.1.12.1 (build 21.0.1+12-LTS, mixed mode, sharing)

```

##### Установка Azul Zulu

```shell
cd /tmp && curl -JLO https://cdn.azul.com/zulu/bin/zulu21.30.15-ca-jdk21.0.1-linux_x64.tar.gz
tar -zxvf zulu21.30.15-ca-jdk21.0.1-linux_x64.tar.gz -C /etc/java && \
cd /etc/java && \
rm -f current && \
ln -s zulu21.30.15-ca-jdk21.0.1-linux_x64 current && \
java --version

```
Проверка новой версии JDK

```shell
root@tomcat-01:/tmp# java --version
openjdk 21.0.1 2023-10-17 LTS
OpenJDK Runtime Environment Zulu21.30+15-CA (build 21.0.1+12-LTS)
OpenJDK 64-Bit Server VM Zulu21.30+15-CA (build 21.0.1+12-LTS, mixed mode, sharing)
```


### Автоматическая установка JDK с использованием архива с бинарными файлами

Для автоматизированной установки JDK и использованием можно воспользоваться скриптом, который автоматически установит JDK на ваш сервер.

#### Доступные версии и сборки

**JDK 8**

- Amazon Corretto
- Azul Zulu

**JDK 11**

- Amazon Corretto
- Azul Zulu

**JDK 17**

- Amazon Corretto
- Azul Zulu
- Oracle GraalVM
- Oracle JDK

**JDK 21**

- Amazon Corretto
- Azul Zulu
- Oracle GraalVM
- Oracle JDK


#### Загрузка и запуска скрипта

Загрузите скрипт на ваш сервер в любую удобную директорию

Сделайте скрипт исполняемый

```shell
chmod +x ./install_jdk.sh
```


Запустить скрипт

```shell
./install_jdk.sh
```

После запуска, скрипт проверит версию вашей операционной системы и установит необходимые зависимости на сервер, после чего предложит выбрать версию JDK, по умолчанию - 21


```shell
root@tomcat-01:/tmp# ./install_jdk_v2.sh 
Обнаружен дистрибутив RHEL/CentOS. Используем dnf для установки зависимостей.
Last metadata expiration check: 4:01:17 ago on Sat 28 Oct 2023 10:44:54 PM +05.
Package curl-7.76.1-23.el9_2.4.x86_64 is already installed.
Package tar-2:1.34-6.el9_1.x86_64 is already installed.
Package jq-1.6-14.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
Доступные версии JDK:
8: JDK 8
21: JDK 21
11: JDK 11
17: JDK 17
Выберите версию JDK (по умолчанию 21, Enter): 

```

Если вам нужна версия 21, то достаточно нажать Enter, если другая, то введите цифру той версии, в текущем примере будет установлена версия 21.
После выбора версии, необходимо выбрать сборку, которая будет скачана с сайта разработчика для дальнейшей установки

```shell
Выберите версию JDK (по умолчанию 21, Enter): 
Доступные сборки JDK для версии 21:
[c]Amazon Corretto, [z]Azul Zulu, [o]Oracle JDK, [g]Oracle GraalVM
Выберите сборку (введите код, например, 'c' для Amazon Corretto), для выхода нажмите 'q': 
```

После выбора сборки, скрипт запустит скачивание архива и его распаковку в директорию `/etc/java`, а также создаст символическую ссылку `/etc/java/current` на эту версию.

```shell
[c]Amazon Corretto, [z]Azul Zulu, [o]Oracle JDK, [g]Oracle GraalVM
Выберите сборку (введите код, например, 'c' для Amazon Corretto), для выхода нажмите 'q': g
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  323M  100  323M    0     0  12.0M      0  0:00:26  0:00:26 --:--:-- 14.0M
Распаковка архива graalvm-jdk-21_linux-x64_bin.tar.gz
Очистка временной директории /tmp
Установка завершена.
Для проверки установленной версии Java введите команду java --version
```


